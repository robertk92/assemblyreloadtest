﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Loader;

namespace AppContextTest;

public class MyAssemblyLoadContext : AssemblyLoadContext
{
    public MyAssemblyLoadContext() : base(isCollectible: true) { }
}

class Program
{
    private static string assemblyFile = "../../../../OtherAssembly/bin/Debug/net8.0/OtherAssembly.dll";

    static void Main(string[] args)
    {
        while (true)
        {
            Console.WriteLine();
            Console.WriteLine("Press 'r' to reload");
            Console.WriteLine();

            ConsoleKeyInfo key = Console.ReadKey();
            if (key.Key == ConsoleKey.R)
            {
                Console.Clear();
                WeakReference<MyAssemblyLoadContext> weakRef = LoadAndPrint();

                if (weakRef.TryGetTarget(out MyAssemblyLoadContext context))
                {
                    if (!context.IsCollectible)
                    {
                        throw new Exception("Can not unload assembly because the context is not collectible");
                    }

                    context.Unload();
                }
                else
                {
                    throw new Exception("Failed to get target from weak reference");
                }
            }
        }
    }

    [MethodImpl(MethodImplOptions.NoInlining)]
    private static WeakReference<MyAssemblyLoadContext> LoadAndPrint()
    {
        MyAssemblyLoadContext loadContext = new MyAssemblyLoadContext();
        using (FileStream stream = File.Open(assemblyFile, FileMode.Open, FileAccess.Read))
        {
            Assembly assembly = loadContext.LoadFromStream(stream);
            PrintClass1Info(assembly);
        }

        return new WeakReference<MyAssemblyLoadContext>(loadContext, trackResurrection:true);
        // loadContext goes out of scope here and is only stored as a weak reference, so it becomes collectible.
    }

    private static void PrintClass1Info(Assembly assembly)
    {
        Type class1 = assembly.GetType("OtherAssembly.Class1");
        if (class1 == null)
        {
            Console.WriteLine("No Class1 found");
            return;
        }

        Console.WriteLine($"class {class1.Name}");
        foreach (FieldInfo field in class1.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
        {
            Console.WriteLine($"\tField: {field.Name}");
        }
    }
}
